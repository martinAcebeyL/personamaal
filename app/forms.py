from flask_wtf import FlaskForm as Form
from wtforms import validators
from wtforms import StringField, HiddenField

#obligado que los atributos sea el formulario y el campo a validar
def length_honeypot(form, field):
    if len(field.data) > 0:
        raise validators.ValidationError('Solo los humanos pueden completar el registro!')

class RegisterForm(Form):
    honeypot = HiddenField("", validators=[ length_honeypot] )
    nombres = StringField("Nombres",validators=[
        validators.length(min=4, max=20),
    ])

    apellidos = StringField("Apellidos",validators=[
        validators.length(min=4, max=20),
    ])