import datetime
from email.policy import default
from flask_login import UserMixin
from . import db

class Persona(db.Model, UserMixin):
    #para cambiar el nombre de la tabla, caso contrario se llamara como el nombre de la clase
    __tablename__="personas"
    #atributos
    id = db.Column(db.Integer, primary_key=True)
    nombres = db.Column(db.String(50), nullable=False)
    apellidos = db.Column(db.String(50), nullable=False)
    date = db.Column(db.DateTime, default=datetime.datetime.now())
    
    @classmethod
    def crear_persona(cls, nombres, apellidos):
        persona = cls(nombres=nombres, apellidos=apellidos)
        db.session.add(persona)
        db.session.commit()
        return persona

    @classmethod
    def get_by_id(cls, id):
        return Persona.query.filter_by(id=id).first()

    def __str__(self):
        return f'nombre: {self.nombres}, apellido: {self.apellidos}'