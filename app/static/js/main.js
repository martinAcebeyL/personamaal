function inventario(){
    $.ajax({
        url:"/inventario",
        data: {
            form:$("form").serialize()
        },
        type:"get",
        contentType:"application/json",
        dataType:"json",
        success:function(data){
            document.getElementById("productos").innerHTML = data['text'];
        }
     });
}