from flask import Blueprint, jsonify
from flask import render_template, request, flash, redirect, url_for
from flask_login import login_user, logout_user, login_required, current_user

from .models import *
from .forms import *
from . import login_manager, db

page = Blueprint("page", __name__)


@login_manager.user_loader
def load_user(id):
   return Persona.get_by_id(id)


@page.route("/", methods=['GET', 'POST'])
def registrarse():
   form = RegisterForm(request.form)
   if request.method == 'POST' and form.validate():
         print('entro')
         persona = Persona.crear_persona(form.nombres.data, form.apellidos.data)
         
         return "<p>registro echo</p>"
   return render_template("auth/register.html", title="Registro", form=form)


@page.route("/logout")
def logout():
   logout_user()
   return redirect(url_for(".login"))
