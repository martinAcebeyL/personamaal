class Config:
    SECRET_KEY = "secret"

class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = "mysql://root:@localhost/bd_examen"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    
configuraciones = {
    "development": DevelopmentConfig,
    "default": DevelopmentConfig, 
}