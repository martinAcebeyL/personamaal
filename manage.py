from flask_script import Manager, Command, Shell
from config import *
from app import create_app, db
config_class = configuraciones["default"]
app = create_app(config_class)

if __name__ == "__main__":
    manager = Manager(app)
    manager.run()